<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => 'test',
        ]);
        $companyUser = User::create([
            'name' => 'Company User',
            'email' => 'company@test.com',
            'password' => 'test',
        ]);
        $company = new \App\Models\Company();
        $company->fill([
            'name'=>'Company1'
        ]);
        $company->user()->associate($companyUser) ;
        $company->save();
        for ($i =1; $i<=10; $i++){
            $lead = new \App\Models\Lead();
            $lead->fill([
                'subject'=>'Subject '.$i,
                'region'=>'Region '.$i,
                'deadline'=> date('Y-m-d'),
                'budget'=>$i,
                'about'=>'About '.$i,
            ]);
            $lead->company()->associate($company) ;
            $lead->save();

            for ($j =1; $j<=10; $j++){
                $task = new \App\Models\Task();
                $task->fill([
                    'name'=>'Name '.$j,
                    'total'=>$j,
                    'done'=> $j-1,
                    'deadline'=>date('Y-m-d'),
                ]);
                $task->lead()->associate($lead) ;
                $task->save();

            }
        }

        $adminRole = Role::findByName(\App\api\Acl::ROLE_ADMIN);
        $companyRole = Role::findByName(\App\api\Acl::ROLE_COMPANY);
        $admin->syncRoles($adminRole);
        $companyUser->syncRoles($companyRole);
        $this->call(UsersTableSeeder::class);
    }
}
