<?php

namespace App\api\Controllers;

use App\api\JsonResponse;
use App\api\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login(Request $request)
	{
		$credentials = $request->only('email', 'password');
		if ($token = auth('api')->attempt($credentials)) {
			$this->guard()->attempt($credentials);

			return response()->json(new JsonResponse(Auth::user()), Response::HTTP_OK)->header('Authorization', $token);
		}

		return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
	}

	public function logout()
	{
		$this->guard()->logout();
		return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
	}

	public function user(Request $request)
	{
		$user = User::find(Auth::user()->id);

		$data = new UserResource( $user );

		return response()->json( new JsonResponse( $data ) );
	}

	/**
	 * @return mixed
	 */
	private function guard()
	{
		return Auth::guard();
	}
	public function refreshToken( ) {
		if($newToken = JWTAuth::parseToken()->refresh()){
			return response()->json(new JsonResponse(Auth::user()), Response::HTTP_OK)->header('Authorization', $newToken );
		}
		return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
	}
	public function changePassword ( Request $request ) {
		$auth = User::findOrFail(Auth::user()->id);
		$validator = Validator::make($request->all(),[
			'current-password' => 'required',
			'password'         => 'required|string|min:5|confirmed',
		] );
		if($validator->fails()){
			return response()->json($validator->errors(),422);
		}
		if ( ! ( Hash::check( $request->get( 'current-password' ), $auth->password ) ) ) {
			// The passwords matches
			return response()->json(new JsonResponse([], 'Your current password does not matches with the password you provided. Please try again.'), Response::HTTP_BAD_REQUEST );
		}
		if ( strcmp( $request->get( 'current-password' ), $request->get( 'password' ) ) == 0 ) {
			//Current password and new password are same
			return response()->json(new JsonResponse([], "New Password cannot be same as your current password. Please choose a different password."), Response::HTTP_BAD_REQUEST );
		}


		$auth->password = bcrypt( $request->get( 'password' ) );
		$auth->save();
		return response()->json(new JsonResponse(["message"=>"Password changed successfully !"]),Response::HTTP_ACCEPTED);
	}
}
