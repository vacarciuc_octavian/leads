<?php

namespace App\api\Controllers;

use App\api\Helpers\CustomArraySerializer;
use App\Api\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\DeviceToken;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiController extends Controller
{
    private $manager;
    public function __construct()
    {
        $this->perPage = 20;
        $this->manager = new Manager();
        $this->manager->setSerializer(new CustomArraySerializer());
    }

    public function index(Request $request){

    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response([
                'data' => [
                    'errors' => [],
                    'message' => 'Invalid Credentials.'
                ]
            ], 400);
        }

        return response(null, 200)
            ->header('Authorization', $token);
    }


    public function user(Request $request)
    {

        $user = User::find(Auth::user()->id);

        $resource = new Item($user, new UserTransformer());
        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ], 200);
    }

    public function refresh()
    {
        return response([], 200);
    }

    public function logout(Request $request)
    {
        JWTAuth::invalidate();

        return response([
            'data' => [
                'message' => 'Logged out Successfully.'
            ]
        ], 200);
    }
}
