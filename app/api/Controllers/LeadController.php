<?php

namespace App\api\Controllers;

use App\api\Helpers\CustomArraySerializer;
use App\Api\Transformers\LeadTransformer;
use App\Api\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\DeviceToken;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Tymon\JWTAuth\Facades\JWTAuth;

class LeadController extends Controller
{
    private $manager;
    public function __construct()
    {
        $this->perPage = 20;
        $this->manager = new Manager();
        $this->manager->setSerializer(new CustomArraySerializer());
    }

    public function list(Request $request){

        $user = User::find(Auth::user()->id);
        if ($user->isAdmin()){
            if ($request->exists('page')) {
                $skipPages = $request->page - 1;
            } else {
                $skipPages = 0;
            }
            $leads = Lead::skip($this->perPage * $skipPages)->take($this->perPage)->get();
        }

        $resource = new Collection($leads, new LeadTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);
    }
    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $this->validate($request, [
//            'subject'=>'',
//            'region'=>'',
//            'deadline'=>'',
//            'budget'=>'',
//            'user_id'=>'',
        ]);

        $lead = new Lead();
        $lead->fill($request->only('subject', 'region', 'deadline', 'budget', 'about'));
        if($user->isAdmin()){
            $lead->company()->associate(Company::where('user_id',$request->user_id)->first());
        }else{
            $lead->company()->associate(Company::where('user_id',$user->id)->first());
        }
        $lead->save();

        $resource = new Item($lead, new LeadTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);

    }
    public function update(Request $request,Lead $lead)
    {
        $user = User::find(Auth::user()->id);
        $this->validate($request, [
            //            'subject'=>'',
            //            'region'=>'',
            //            'deadline'=>'',
            //            'budget'=>'',
            //            'user_id'=>'',
        ]);

        $lead->fill($request->only('subject', 'region', 'deadline', 'budget', 'about'));
        if($user->isAdmin()){
            $lead->company()->associate(Company::where('user_id',$request->user_id)->first());
        }else{
            $lead->company()->associate(Company::where('user_id',$user->id)->first());
        }
        $lead->save();

        $resource = new Item($lead, new LeadTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);

    }
    public function delete(Request $request, Lead $lead)
    {
        $user = User::find(Auth::user()->id);
        if($user->isAdmin()){
            $lead->delete();
        }else{
            $user->companies()->first()->leads()->where('id', $lead->id)->delete();
        }
        return response(null, 200);
    }
}
