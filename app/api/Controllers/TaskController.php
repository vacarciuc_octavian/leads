<?php

namespace App\api\Controllers;

use App\api\Helpers\CustomArraySerializer;
use App\Api\Transformers\TaskTransformer;
use App\Api\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\DeviceToken;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Tymon\JWTAuth\Facades\JWTAuth;

class TaskController extends Controller
{
    private $manager;
    public function __construct()
    {
        $this->perPage = 20;
        $this->manager = new Manager();
        $this->manager->setSerializer(new CustomArraySerializer());
    }

    public function list(Request $request){

        $user = User::find(Auth::user()->id);
        if ($user->isAdmin()){
            if ($request->exists('page')) {
                $skipPages = $request->page - 1;
            } else {
                $skipPages = 0;
            }
            $tasks = Task::skip($this->perPage * $skipPages)->take($this->perPage)->get();
        }

        $resource = new Collection($tasks, new TaskTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);
    }
    public function add(Request $request,Lead $lead)
    {
        $user = User::find(Auth::user()->id);
        $this->validate($request, [
//            'name'=>'',
//            'total'=>'',
//            'done'=>'',
//            'deadline'=>'',
        ]);

        $task = new Task();
        $task->fill($request->only('name', 'total', 'done', 'deadline'));
        if($user->isAdmin()){
            $task->lead()->associate($lead);
        }else{
            if($lead->company->user_id == $user->id){
                $task->lead()->associate($lead);
            }else{
                return response(null,401);
            }
        }
        $task->save();

        $resource = new Item($task, new TaskTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);

    }
    public function update(Request $request,Lead $lead,Task $task)
    {
        $user = User::find(Auth::user()->id);
        $this->validate($request, [
            //            'name'=>'',
            //            'total'=>'',
            //            'done'=>'',
            //            'deadline'=>'',
        ]);

        $task->fill($request->only('name', 'total', 'done', 'deadline'));
        if($user->isAdmin()){
            $task->lead()->associate($lead);
        }else{
            if($lead->company->user_id == $user->id){
                $task->lead()->associate($lead);
            }else{
                return response(null,401);
            }
        }
        $task->save();

        $resource = new Item($task, new TaskTransformer());

        return response([
            'data' => $this->manager->createData($resource)->toArray()
        ]);

    }
    public function delete(Request $request, Task $task)
    {
        $user = User::find(Auth::user()->id);
        if($user->isAdmin()){
            $task->delete();
        }else{
            $leads_ids = $user->companies()->first()->leads()->pluck('id')->toArray();
            Task::whereIn('lead_id', $leads_ids)->delete();
        }
        return response(null, 200);
    }
}
