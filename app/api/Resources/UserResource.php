<?php

namespace App\api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
		$avatar = $this->getFirstMedia( 'avatar' );

		return [
			'id'                   => $this->id,
			'name'                 => $this->name,
			'username'             => $this->username,
			'uid'                  => $this->uid,
			'email'                => $this->email,
			'role'                 => $this->role,
			'avatar'               => ! is_null( $avatar ) ? $avatar->getUrl() : '',
			'contact_phones'       => (string) $this->contact_phone,
			'contact_phones_array' => explode( "\n", $this->contact_phone ),
			'location_lat'         => $this->location_lat,
			'location_long'        => $this->location_long,
			'time_to_next_point'   => $this->time_to_next_point,

			'created_at' => $this->created_at,
		];

	}
}
