<?php


namespace App\Api\Transformers;

use App\api\Helpers\CustomArraySerializer;
use App\Models\Lead;
use App\Models\Room;
use App\Models\Task;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{

    public function __construct($gtm = 0)
    {
        $this->gtm = $gtm;
    }

    public function transform(Task $task)
    {

        $manager = new Manager();
        $manager->setSerializer(new CustomArraySerializer());
        return [
            'id' => (int)$task->id,
            'name' => (string)$task->name,
            'total' => (int)$task->total,
            'done' => (int)$task->done,
            'deadline' => (string)$task->deadline,
        ];
    }
}
