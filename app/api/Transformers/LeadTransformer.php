<?php


namespace App\Api\Transformers;

use App\api\Helpers\CustomArraySerializer;
use App\Models\Lead;
use App\Models\Room;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class LeadTransformer extends TransformerAbstract
{

    public function __construct($gtm = 0)
    {
        $this->gtm = $gtm;
    }

    public function transform(Lead $lead)
    {

        $manager = new Manager();
        $manager->setSerializer(new CustomArraySerializer());
        return [
            'id' => (int)$lead->id,
            'about' => (string)$lead->about,
            'subject' => (string)$lead->subject,
            'region' => (string)$lead->region,
            'deadline' => (string)$lead->deadline,
            'budget' => (float)$lead->budget,
            'created_at' => $lead->created_at->toDateString(),
        ];
    }
}
