<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 27.03.2017
 * Time: 17:38
 */

namespace App\Api\Transformers;


use App\api\Helpers\CustomArraySerializer;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    public function transform(\App\Models\User $user) {
        $manager = new Manager();
        $manager->setSerializer(new CustomArraySerializer());
        return [
            'id'			    => (int)    $user->id,
            'name'				=> (string) $user->name,
            'username'		    => (string) $user->username,
            'email'				=> (string) $user->email,
            'city'		        => (string) $user->city,
            'country'		    => (string) $user->country,
        ];
    }
}
