<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int            $id
 */
class Company extends Model
{
    protected $fillable = [
        'name','about', 'phone', 'email', 'address', 'category', 'presentation'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function leads() {
        return $this->hasMany(Lead::class);
    }
//    public function de
}
