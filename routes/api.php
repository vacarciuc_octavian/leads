<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//DeviceToken Endpoints
Route::post('setDeviceToken', '\App\api\Controllers\ApiController@setDeviceToken');
Route::post('removeDeviceToken', '\App\api\Controllers\ApiController@removeDeviceToken');

Route::post('auth/register', '\App\api\Controllers\ApiController@register');
Route::post('auth/login', '\App\api\Controllers\ApiController@login');

Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', '\App\api\Controllers\ApiController@refresh');
});
Route::group([ 'middleware' => 'jwt.auth'], function () {
    Route::get('auth/user', '\App\api\Controllers\ApiController@user');
    Route::post('auth/logout', '\App\api\Controllers\ApiController@logout');

    Route::put('auth/update', '\App\api\Controllers\ApiController@update');

    Route::get('leads', '\App\api\Controllers\LeadController@list');
    Route::post('leads', '\App\api\Controllers\LeadController@add');
    Route::put('leads/{lead}', '\App\api\Controllers\LeadController@update');
    Route::delete('leads/{lead}', '\App\api\Controllers\LeadController@delete');

    Route::get('tasks', '\App\api\Controllers\TaskController@list');
    Route::post('tasks/{lead}', '\App\api\Controllers\TaskController@add');
    Route::put('tasks/{lead}/{task}', '\App\api\Controllers\TaskController@update');
    Route::delete('tasks/{task}', '\App\api\Controllers\TaskController@delete');

});
